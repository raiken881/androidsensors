# Android Sensors Integration Steps #

This file demonstrates how to connect the android phone to the laptop and enable usb transfer using ADB.

Steps:

* Download android studio.(Any version is fine).

* Launch android studio, navigate to SDK Manager and click on "launch the SDK manager"(see pictures below)
![SDKManagerNavigation.png](https://bitbucket.org/repo/e4eqq6/images/220151448-SDKManagerNavigation.png)
![LaunchSDKManager.png](https://bitbucket.org/repo/e4eqq6/images/3471702959-LaunchSDKManager.png)

* Download the packages "Android SDK platform tools" and "Google USB Driver" if you don't have these packages.

* Once done, note down the path where android installed the packages. This path can be found from the SDK Manager at the top with a label named "SDK Path".

* Enable USB debugging from the phone and connect it to the terminal. Let windows install the drivers.

* Navigate to the path where android installed the packages downloaded and there should be a folder named "platform-tools".

* Hold shift and right click on it and select "Open command window here".

* From the newly opened terminal, type in "adb forward tcp:9000 tcp:9000". It should work after calibrating and connecting to the speed sensor.

* Now that the port has been forwarded onto the phone's port, check if the phone's internal magnetic sensor is calibrated. To do that go to the phone app and type in "\*#0\*#"(Only works for Samsung phones). This will open the hardware center of the phone. There is an option with "Sensor" marked on it. Select that option and locate the magnetic sensor heading in that page. There will be a circle and a value in it(see picture below). If the value is 0,1 or 2 then the device needs calibration. To calibrate it, do the figure 8 until the value goes to 3.

![Proximity_issue_2.png](https://bitbucket.org/repo/e4eqq6/images/2108313769-Proximity_issue_2.png)

* After that calibration step, go to the app named "Home Activity" located on the phone. There will be 2 options, namely "Calibrate" and "Start rift"

* Start by selecting the "Calibrate" option. Make the user turn the handlebars completely to the left then press "Ok". Then make him/her turn the handlebars completely to the right and press "OK". Then press "Finish"

* Next, select the "Start rift option". A list of sensors will be displayed but since the speed sensor is needed, select the "speed sensor" option. For the phone to find the speed sensor, rotate the back wheel until the sensor goes into an awake state and is visible on the phone. The sensor will come up with the name of "test". Select it and there will be a table showing all the data outputted by the sensor.

* Now start Unity and enjoy the game!