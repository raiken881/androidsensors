package comp5216.sydney.edu.au.thesiscalibration;

import android.os.Debug;
import android.util.Log;

import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Noobs on 11/26/2015.
 */
public class SocketServer implements Runnable
{

    ServerSocket server = null;
    Socket client = null;
    OutputStream output = null;
    Thread backgroundServer = null;


    /**
     * This function is used whenever the sending of data failed or the application quits.
     * It closes any open stream and set the thread to null if it needs to be reused
     * */
    public void releaseResources()
    {
        try
        {
            if(server !=null && client !=null)
            {
                server.close();
                client.close();
                output.close();
                backgroundServer = null;
                client = null;
                server = null;
                output= null;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**Check if the client is still connected to us  */
    public boolean connected()
    {
        if(client != null)
        {
            if(client.isConnected())
            {
                return true;

            }
            else
            {
                Log.i("FinalThesisCalibration","Client is not connected");
                return false;
            }
        }
        return false;
    }



    public Thread getBackgroundThread()
    {
        return this.backgroundServer;
    }

    public void setBackgroundServerToNull()
    {
        this.backgroundServer = null;
    }

    /** start the thread */
    public void runServer()
    {
        if(this.backgroundServer == null)
        {
            Log.i("FinalThesisCalibration", "Reaches here");
            backgroundServer = new Thread(this);
            backgroundServer.start();
        }
    }

    /**
     * This function is used to send data through the socket. If it fails to then the application releases any resources
     * and restart the thread so that it starts listening if the client is back online.
     *
     * @param data The data to be sent to unity.
     * */
    public void sendData(String data)
    {

        if(client != null && server !=null && output !=null)
        {


            try
            {
                if(client.isConnected())
                {

                    output.write(data.getBytes());
                    output.flush();

                }
            }
            catch (Exception e)
            {
                releaseResources();
                e.printStackTrace();
                backgroundServer = new Thread(this);
                backgroundServer.start();
            }
        }
    }

    /**Wait until the client is connected, then create an output stream object so that the data can be sent
     * Here the port 9000 is the one we used for the project. If the adb command is used on the laptop with a different
     * port number, then change the port number to the one forwarded.
     * */
    @Override
    public void run() {

        try {
            server = new ServerSocket(9000);
            while (true) {
                if(client != null && client.isClosed())
                {

                    client = null;
                    break;
                }
                client = server.accept();

                if(client.isConnected())
                {
                    break;
                }
            }

            if( client !=null)
            {
                Log.i("FinalThesisCalibration","Got a client");
                output = client.getOutputStream();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}

