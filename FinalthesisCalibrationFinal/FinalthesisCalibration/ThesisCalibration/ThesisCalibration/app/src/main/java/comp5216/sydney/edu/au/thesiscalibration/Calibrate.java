package comp5216.sydney.edu.au.thesiscalibration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Calibrate extends AppCompatActivity implements SensorEventListener {


    TextView myTextView;
    ImageView compass;

    TextView handleBar;
    SensorManager manager;
    boolean leftCalibrated = false;
    boolean rightCalibrated = false;
    Button finishButton;
    Button capture;

    private float leftValue = 0.0f;



    private float currentDeg = 0;

    float marginError = 3f;
    float firstquat[] =new float[4];
    float rightValue=0.0f;
    static  final float ALPHA = 0.15f;

    //To be used later to retrieve accelerometer and magnetic sensor values
    float [] mRotationVect = new float[3] ;




    private float finalSensorVal = 0.0f;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibrate_right);
        handleBar = (TextView)findViewById(R.id.handleBarText);
        compass = (ImageView)findViewById(R.id.compass);
        compass.setBackgroundResource(R.drawable.img_compass);
        myTextView = (TextView) findViewById(R.id.rotateAngle);
        finishButton = (Button)findViewById(R.id.finish);
        finishButton.setVisibility(View.GONE);
        capture = (Button)findViewById(R.id.next);
        manager = (SensorManager) getSystemService(SENSOR_SERVICE);

    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this, manager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_FASTEST);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calibrate_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {





        if(event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR)
        {
            mRotationVect = event.values.clone();

            finalSensorVal = getSensorVal(mRotationVect);
            myTextView.setText(Float.toString(finalSensorVal));
            rotateImage(finalSensorVal);

        }
    }

    private void rotateImage(float azimuth)
    {
        RotateAnimation ra = new RotateAnimation(
                currentDeg,
                -azimuth,
                Animation.RELATIVE_TO_SELF,0.5f,
                Animation.RELATIVE_TO_SELF,0.5f
        );
        ra.setDuration(210);
        ra.setFillAfter(true);
        compass.startAnimation(ra);
        currentDeg = -azimuth;

    }



    private float getSensorVal(float[] values) {
        float rotation[] = new float[9];
        manager.getRotationMatrixFromVector(rotation,values);
        float orientation[] = new float[3];
        manager.getOrientation(rotation, orientation);
        if (orientation[0] < 0) {
            return (float) (Math.round(Math.toDegrees(orientation[0]) + 360));
        } else {
            return (float) Math.round(Math.toDegrees(orientation[0]));
        }
    }

    private float[] lowPass(float[]input,float[]output)
    {
        if(output == null)
        {
            return input;
        }
       for(int i = 0 ;i < input.length;i++)
       {
           output[i] = output[i] + ALPHA * (input[i] - output[i]);
       }
        return output;
    }



    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }



    public void startLeftCalibration(View v)
    {

        if(!leftCalibrated)
        {
            leftCalibrated = true;
            leftValue = finalSensorVal;
            handleBar.setText("Turn handle bar completely right");
            Toast.makeText(this,Float.toString(leftValue),Toast.LENGTH_LONG).show();
            return;

        }

        if(!rightCalibrated)
        {
            rightCalibrated = true;
            rightValue = finalSensorVal;
            handleBar.setText("Click finish to proceed");
            Toast.makeText(this,Float.toString(rightValue),Toast.LENGTH_LONG).show();
            finishButton.setVisibility(View.VISIBLE);
            capture.setVisibility(View.GONE);
        }


    }


    public void finish(View v)
    {
        Intent intent = new Intent();

        SharedPreferences preferences = this.getSharedPreferences(getString(R.string.SharedPreference),this.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putFloat(getString(R.string.rightCalib), rightValue);


        intent.putExtra("Success", finalSensorVal);
        float magnitude;
        float offset;


        if(leftValue > rightValue)
        {
            leftValue -= 360;
            magnitude = (rightValue - leftValue)/2;
            offset = leftValue + magnitude;

        }
        else {
            magnitude = (rightValue - leftValue)/2;
            offset = leftValue + magnitude;
        }
        Log.i("Thesis","left is: " + leftValue);
        Log.i("Thesis","right is:" + rightValue);

        editor.putFloat(getString(R.string.magnitude),magnitude);
        editor.putFloat(getString(R.string.offset),offset);
        editor.putFloat(getString(R.string.leftCalib),leftValue);
        editor.apply();
        setResult(RESULT_OK);
        finish();
    }
}
