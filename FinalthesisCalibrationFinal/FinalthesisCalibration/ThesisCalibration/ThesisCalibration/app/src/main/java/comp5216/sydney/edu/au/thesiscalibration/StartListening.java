package comp5216.sydney.edu.au.thesiscalibration;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeCadencePcc;
import com.dsi.ant.plugins.antplus.pcc.AntPlusBikeSpeedDistancePcc;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;

import java.math.BigDecimal;
import java.util.EnumSet;

/**
 * This class is used to capture any angle change values, speed values and send these data to Unity.
 * NOTE: To collect the speed data from the speed sensor, the ANT+ API is used. I commented out the
 * functions that the API offer if someone else uses this program.
 *
 * */

public class StartListening extends Activity implements SensorEventListener {

    TextView tv_status;

    TextView tv_estTimestamp;

    TextView tv_calculatedSpeed;
    TextView tv_calculatedAccumulatedDistance;

    TextView tv_cumulativeRevolutions;
    TextView tv_timestampOfLastEvent;

    TextView tv_isSpdAndCadCombo;
    TextView tv_calculatedCadence;

    TextView tv_cumulativeOperatingTime;

    TextView tv_manufacturerID;
    TextView tv_serialNumber;

    TextView tv_hardwareVersion;
    TextView tv_softwareVersion;
    TextView tv_modelNumber;


    TextView textView_BatteryVoltage;
    TextView textView_BatteryStatus;

    TextView textView_IsStopped;

    private static SensorManager manager;
    public boolean connectionSuccess = false;
    public long currentTime = System.currentTimeMillis();
    public final int delay = 200;
    public long sensorTime;
    private long currentRevolutions = 0;
    private long previousRevolutions = -1;
    public int revolution = 0;


    float magnitude;
    float left;
    float offset;
    private float angle = 0f;


    SocketServer server;
    Thread backgroundServer;

    AntPlusBikeSpeedDistancePcc bsdPcc = null;
    PccReleaseHandle<AntPlusBikeSpeedDistancePcc> bsdReleaseHandle = null;
    AntPlusBikeCadencePcc bcPcc = null;
    PccReleaseHandle<AntPlusBikeCadencePcc> bcReleaseHandle = null;



    AntPluginPcc.IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc> mResultReceiver = new AntPluginPcc.IPluginAccessResultReceiver<AntPlusBikeSpeedDistancePcc>() {
        // Handle the result, connecting to events on success or reporting
        // failure to user.
        //If the connection has been successful, then the whole program starts i.e the sensor can start capturing the values from the compass
        @Override
        public void onResultReceived(AntPlusBikeSpeedDistancePcc result,
                                     RequestAccessResult resultCode, DeviceState initialDeviceState) {
            switch (resultCode) {
                case SUCCESS:
                    bsdPcc = result;
                    tv_status.setText(result.getDeviceName() + ": " + initialDeviceState);
                    subscribeToEvents();
                    connectionSuccess = true;
                    break;
                case CHANNEL_NOT_AVAILABLE:
                    Toast.makeText(StartListening.this, "Channel Not Available",
                            Toast.LENGTH_SHORT).show();
                    tv_status.setText(R.string.reset_conn);
                    break;
                case ADAPTER_NOT_DETECTED:
                    Toast
                            .makeText(
                                    StartListening.this,
                                    "ANT Adapter Not Available. Built-in ANT hardware or external adapter required.",
                                    Toast.LENGTH_SHORT).show();
                    tv_status.setText(R.string.reset_conn);
                    break;
                case BAD_PARAMS:
                    // Note: Since we compose all the params ourself, we should
                    // never see this result
                    Toast.makeText(StartListening.this,
                            "Bad request parameters.", Toast.LENGTH_SHORT).show();
                    tv_status.setText(R.string.reset_conn);
                    break;
                case OTHER_FAILURE:
                    Toast.makeText(StartListening.this,
                            "RequestAccess failed. See logcat for details.", Toast.LENGTH_SHORT)
                            .show();
                    tv_status.setText(R.string.reset_conn);
                    break;
                case DEPENDENCY_NOT_INSTALLED:
                    tv_status.setText(R.string.reset_conn);
                    AlertDialog.Builder adlgBldr = new AlertDialog.Builder(
                            StartListening.this);
                    adlgBldr.setTitle("Missing Dependency");
                    adlgBldr
                            .setMessage("The required service\n\""
                                    + AntPlusBikeSpeedDistancePcc.getMissingDependencyName()
                                    + "\"\n was not found. You need to install the ANT+ Plugins service or you may need to update your existing version if you already have it. Do you want to launch the Play Store to get it?");
                    adlgBldr.setCancelable(true);
                    adlgBldr.setPositiveButton("Go to Store", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent startStore = null;
                            startStore = new Intent(Intent.ACTION_VIEW, Uri
                                    .parse("market://details?id="
                                            + AntPlusBikeSpeedDistancePcc
                                            .getMissingDependencyPackageName()));
                            startStore.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            StartListening.this.startActivity(startStore);
                        }
                    });
                    adlgBldr.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    final AlertDialog waitDialog = adlgBldr.create();
                    waitDialog.show();
                    break;
                case USER_CANCELLED:
                    tv_status.setText(R.string.reset_conn);
                    break;
                case UNRECOGNIZED:
                    Toast.makeText(StartListening.this,
                            "Failed: UNRECOGNIZED. PluginLib Upgrade Required?",
                            Toast.LENGTH_SHORT).show();
                    tv_status.setText(R.string.reset_conn);
                    break;
                default:
                    Toast.makeText(StartListening.this,
                            "Unrecognized result: " + resultCode, Toast.LENGTH_SHORT).show();
                    tv_status.setText(R.string.reset_conn);
                    break;
            }
        }

        /**
         * Subscribe to all the revolution events, connecting them to display
         * their data.
         */
        private void subscribeToEvents() {

            //This part is used to get the revolution. ANT + plugin provides a way to calculate the revolution by using a single funciton
            /*
            // 2.095m circumference = an average 700cx23mm road tire
            bsdPcc.subscribeCalculatedSpeedEvent(new AntPlusBikeSpeedDistancePcc.CalculatedSpeedReceiver(new BigDecimal(2.095))
            {
                @Override
                public void onNewCalculatedSpeed(final long estTimestamp,
                                                 final EnumSet<EventFlag> eventFlags, final BigDecimal calculatedSpeed)
                {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            tv_estTimestamp.setText(String.valueOf(estTimestamp));

                            tv_calculatedSpeed.setText(String.valueOf(calculatedSpeed));
                        }
                    });
                }
            });

            bsdPcc
                    .subscribeCalculatedAccumulatedDistanceEvent(new AntPlusBikeSpeedDistancePcc.CalculatedAccumulatedDistanceReceiver(
                            new BigDecimal(2.095)) // 2.095m circumference = an average
                            // 700cx23mm road tire
                    {

                        @Override
                        public void onNewCalculatedAccumulatedDistance(final long estTimestamp,
                                                                       final EnumSet<EventFlag> eventFlags,
                                                                       final BigDecimal calculatedAccumulatedDistance)
                        {
                            runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    tv_estTimestamp.setText(String.valueOf(estTimestamp));

                                    tv_calculatedAccumulatedDistance.setText(String
                                            .valueOf(calculatedAccumulatedDistance.setScale(3,
                                                    RoundingMode.HALF_UP)));
                                }
                            });
                        }
                    });
                    */

            /**
             * This function is one of ANT+ API functions used to get the cumulative revolutions. If the current revolution
             * is greater than the previous revolution then set the revolution to be sent to 1 else set it to 0.
             *
             * */
            bsdPcc.subscribeRawSpeedAndDistanceDataEvent(new AntPlusBikeSpeedDistancePcc.IRawSpeedAndDistanceDataReceiver() {
                @Override
                public void onNewRawSpeedAndDistanceData(final long estTimestamp,
                                                         final EnumSet<EventFlag> eventFlags,
                                                         final BigDecimal timestampOfLastEvent, final long cumulativeRevolutions) {


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (previousRevolutions == -1) {
                                previousRevolutions = cumulativeRevolutions;
                                currentRevolutions = cumulativeRevolutions;
                            }
                            if (cumulativeRevolutions > previousRevolutions) {
                                revolution = 1;
                            } else {
                                revolution = 0;
                            }
                            previousRevolutions = cumulativeRevolutions;

                            tv_estTimestamp.setText(String.valueOf(estTimestamp));
                            tv_timestampOfLastEvent.setText(String.valueOf(timestampOfLastEvent));
                            tv_cumulativeRevolutions.setText(String.valueOf(cumulativeRevolutions));
                        }
                    });

                }
            });


            /*
            if (bsdPcc.isSpeedAndCadenceCombinedSensor())
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        tv_isSpdAndCadCombo.setText("Yes");
                        tv_cumulativeOperatingTime.setText("N/A");
                        tv_manufacturerID.setText("N/A");
                        tv_serialNumber.setText("N/A");
                        tv_hardwareVersion.setText("N/A");
                        tv_softwareVersion.setText("N/A");
                        tv_modelNumber.setText("N/A");

                        tv_calculatedCadence.setText("...");

                        bcReleaseHandle = AntPlusBikeCadencePcc.requestAccess(
                                Speed.this,
                                bsdPcc.getAntDeviceNumber(), 0, true,
                                new IPluginAccessResultReceiver<AntPlusBikeCadencePcc>()
                                {
                                    // Handle the result, connecting to events
                                    // on success or reporting failure to user.
                                    @Override
                                    public void onResultReceived(AntPlusBikeCadencePcc result,
                                                                 RequestAccessResult resultCode,
                                                                 DeviceState initialDeviceStateCode)
                                    {
                                        switch (resultCode)
                                        {
                                            case SUCCESS:
                                                bcPcc = result;
                                                bcPcc
                                                        .subscribeCalculatedCadenceEvent(new ICalculatedCadenceReceiver()
                                                        {
                                                            @Override
                                                            public void onNewCalculatedCadence(
                                                                    long estTimestamp,
                                                                    EnumSet<EventFlag> eventFlags,
                                                                    final BigDecimal calculatedCadence)
                                                            {
                                                                runOnUiThread(new Runnable()
                                                                {
                                                                    @Override
                                                                    public void run()
                                                                    {
                                                                        tv_calculatedCadence.setText(String
                                                                                .valueOf(calculatedCadence));
                                                                    }
                                                                });
                                                            }
                                                        });
                                                break;
                                            case CHANNEL_NOT_AVAILABLE:
                                                tv_calculatedCadence
                                                        .setText("CHANNEL NOT AVAILABLE");
                                                break;
                                            case BAD_PARAMS:
                                                tv_calculatedCadence.setText("BAD_PARAMS");
                                                break;
                                            case OTHER_FAILURE:
                                                tv_calculatedCadence.setText("OTHER FAILURE");
                                                break;
                                            case DEPENDENCY_NOT_INSTALLED:
                                                tv_calculatedCadence
                                                        .setText("DEPENDENCY NOT INSTALLED");
                                                break;
                                            default:
                                                tv_calculatedCadence.setText("UNRECOGNIZED ERROR: "
                                                        + resultCode);
                                                break;
                                        }
                                    }
                                },
                                // Receives state changes and shows it on the
                                // status display line
                                new IDeviceStateChangeReceiver()
                                {
                                    @Override
                                    public void onDeviceStateChange(final DeviceState newDeviceState)
                                    {
                                        runOnUiThread(new Runnable()
                                        {
                                            @Override
                                            public void run()
                                            {
                                                if (newDeviceState != DeviceState.TRACKING)
                                                    tv_calculatedCadence.setText(newDeviceState
                                                            .toString());
                                                if (newDeviceState == DeviceState.DEAD)
                                                    bcPcc = null;
                                            }
                                        });

                                    }
                                });
                    }
                });
            }

            else
            {
                // Subscribe to the events available in the pure cadence profile
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        tv_isSpdAndCadCombo.setText("No");
                        tv_calculatedCadence.setText("N/A");
                    }
                });

                bsdPcc.subscribeCumulativeOperatingTimeEvent(new ICumulativeOperatingTimeReceiver()
                {
                    @Override
                    public void onNewCumulativeOperatingTime(final long estTimestamp,
                                                             final EnumSet<EventFlag> eventFlags, final long cumulativeOperatingTime)
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                tv_estTimestamp.setText(String.valueOf(estTimestamp));

                                tv_cumulativeOperatingTime.setText(String
                                        .valueOf(cumulativeOperatingTime));
                            }
                        });
                    }
                });

                bsdPcc.subscribeManufacturerAndSerialEvent(new IManufacturerAndSerialReceiver()
                {
                    @Override
                    public void onNewManufacturerAndSerial(final long estTimestamp,
                                                           final EnumSet<EventFlag> eventFlags, final int manufacturerID,
                                                           final int serialNumber)
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                tv_estTimestamp.setText(String.valueOf(estTimestamp));

                                tv_manufacturerID.setText(String.valueOf(manufacturerID));
                                tv_serialNumber.setText(String.valueOf(serialNumber));
                            }
                        });
                    }
                });

                bsdPcc.subscribeVersionAndModelEvent(new IVersionAndModelReceiver()
                {
                    @Override
                    public void onNewVersionAndModel(final long estTimestamp,
                                                     final EnumSet<EventFlag> eventFlags, final int hardwareVersion,
                                                     final int softwareVersion, final int modelNumber)
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                tv_estTimestamp.setText(String.valueOf(estTimestamp));

                                tv_hardwareVersion.setText(String.valueOf(hardwareVersion));
                                tv_softwareVersion.setText(String.valueOf(softwareVersion));
                                tv_modelNumber.setText(String.valueOf(modelNumber));
                            }
                        });
                    }
                });

                bsdPcc.subscribeBatteryStatusEvent(new IBatteryStatusReceiver()
                {
                    @Override
                    public void onNewBatteryStatus(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                                                   final BigDecimal batteryVoltage, final BatteryStatus batteryStatus)
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                tv_estTimestamp.setText(String.valueOf(estTimestamp));

                                textView_BatteryVoltage.setText(batteryVoltage.intValue() != -1 ? String.valueOf(batteryVoltage) + "V" : "Invalid");
                                textView_BatteryStatus.setText(batteryStatus.toString());
                            }
                        });
                    }
                });

                bsdPcc.subscribeMotionAndSpeedDataEvent(new IMotionAndSpeedDataReceiver()
                {
                    @Override
                    public void onNewMotionAndSpeedData(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                                                        final boolean isStopped)
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                tv_estTimestamp.setText(String.valueOf(estTimestamp));

                                textView_IsStopped.setText(String.valueOf(isStopped));
                            }
                        });
                    }
                });
            }
            */
        }
    };

    // Receives state changes and shows it on the status display line
    AntPluginPcc.IDeviceStateChangeReceiver mDeviceStateChangeReceiver = new AntPluginPcc.IDeviceStateChangeReceiver() {
        @Override
        public void onDeviceStateChange(final DeviceState newDeviceState) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tv_status.setText(bsdPcc.getDeviceName() + ": " + newDeviceState);
                    if (newDeviceState == DeviceState.DEAD)
                        bsdPcc = null;
                }
            });
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_listening);
        tv_status = (TextView) findViewById(R.id.textView_Status);

        tv_estTimestamp = (TextView) findViewById(R.id.textView_EstTimestamp);

        tv_calculatedSpeed = (TextView) findViewById(R.id.textView_CalculatedSpeed);
        tv_calculatedAccumulatedDistance = (TextView) findViewById(R.id.textView_CalculatedAccumulatedDistance);
        tv_cumulativeRevolutions = (TextView) findViewById(R.id.textView_CumulativeRevolutions);
        tv_timestampOfLastEvent = (TextView) findViewById(R.id.textView_TimestampOfLastEvent);

        tv_isSpdAndCadCombo = (TextView) findViewById(R.id.textView_IsCombinedSensor);
        tv_calculatedCadence = (TextView) findViewById(R.id.textView_CalculatedCadence);

        tv_cumulativeOperatingTime = (TextView) findViewById(R.id.textView_CumulativeOperatingTime);

        tv_manufacturerID = (TextView) findViewById(R.id.textView_ManufacturerID);
        tv_serialNumber = (TextView) findViewById(R.id.textView_SerialNumber);

        tv_hardwareVersion = (TextView) findViewById(R.id.textView_HardwareVersion);
        tv_softwareVersion = (TextView) findViewById(R.id.textView_SoftwareVersion);
        tv_modelNumber = (TextView) findViewById(R.id.textView_ModelNumber);

        textView_BatteryVoltage = (TextView) findViewById(R.id.textView_BatteryVoltage);
        textView_BatteryStatus = (TextView) findViewById(R.id.textView_BatteryStatus);

        textView_IsStopped = (TextView) findViewById(R.id.textView_IsStopped);

        SharedPreferences preferences = this.getSharedPreferences(getString(R.string.SharedPreference), this.MODE_PRIVATE);

        left = preferences.getFloat(getString(R.string.leftCalib), 0f);
        magnitude = preferences.getFloat(getString(R.string.magnitude), 0f);
        offset = preferences.getFloat(getString(R.string.offset), 0f);
        Log.i("Thesis", "Offset is" + offset);
        Log.i("Thesis", "Magnitude is" + magnitude);
        manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        resetPcc();

        server = new SocketServer();
        server.runServer();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        manager.unregisterListener(this);
        server.releaseResources();


        bsdReleaseHandle.close();
        if (bcReleaseHandle != null) {
            bcReleaseHandle.close();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        manager.registerListener(this, manager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_FASTEST);

    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);

        server.releaseResources();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.reset_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.menu_reset:
                resetPcc();
                tv_status.setText("Resetting...");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }


    /**
     * Resets the PCC connection to request access again and clears any existing display data.
     */
    private void resetPcc() {
        //Release the old access if it exists
        if (bsdReleaseHandle != null) {
            bsdReleaseHandle.close();
        }


        //Reset the text display
        tv_status.setText("Connecting...");

        tv_estTimestamp.setText("---");

        tv_calculatedSpeed.setText("---");
        tv_calculatedAccumulatedDistance.setText("---");
        tv_cumulativeRevolutions.setText("---");
        tv_timestampOfLastEvent.setText("---");

        tv_isSpdAndCadCombo.setText("---");
        tv_calculatedCadence.setText("---");

        tv_cumulativeOperatingTime.setText("---");

        tv_manufacturerID.setText("---");
        tv_serialNumber.setText("---");

        tv_hardwareVersion.setText("---");
        tv_softwareVersion.setText("---");
        tv_modelNumber.setText("---");

        textView_BatteryVoltage.setText("---");
        textView_BatteryStatus.setText("---");

        textView_IsStopped.setText("---");


        // starts the plugins UI search
        bsdReleaseHandle = AntPlusBikeSpeedDistancePcc.requestAccess(this, this,
                mResultReceiver, mDeviceStateChangeReceiver);

    }

    /**
     * Apply the algorithm using the offset and magnitude to determine where the current normalised heading value lies
     * on the number line of -1 to 1. Once that heading value determined, the current revolution along with the angle of
     * rotation is sent to unity via sockets. Also add a small delay to let the data be sent to unity.
     * */
    @Override
    public void onSensorChanged(SensorEvent event) {
        sensorTime = System.currentTimeMillis();
        if((sensorTime - currentTime) >= delay) {
            currentTime = sensorTime;
            if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
                angle = getSensorVal(event.values);
                if (angle - offset > magnitude) {
                    angle = angle - 360;
                    angle = (angle - offset) / magnitude;

                } else {
                    angle = (angle - offset) / magnitude;
                }

                if (server.connected()) {
                    server.sendData(angle + "," + revolution);


                }
            }
        }



    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {


    }

    /**
     * This function calculates the orientation of the device by first extracting the rotation matrix
     * from the rotation vector sensor values then using the sensor manager API to get the orientation.
     * @param mRotationVect values from the rotation vector sensor
     * @return returns the azimuth value of the orientation in degrees.
     * */
    private float getSensorVal(float[] mRotationVect) {
        float rotation[] = new float[9];
        manager.getRotationMatrixFromVector(rotation, mRotationVect);
        float orientation[] = new float[3];
        manager.getOrientation(rotation, orientation);
        if (orientation[0] < 0) {
            return (float) (Math.round(Math.toDegrees(orientation[0]) + 360));
        } else {
            return (float) Math.round(Math.toDegrees(orientation[0]));
        }
    }


}
