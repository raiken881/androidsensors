package comp5216.sydney.edu.au.thesiscalibration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    Button calibrateRight;
    Button finish;

    public static final int GET_RIGHT = 300;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        calibrateRight = (Button)findViewById(R.id.calibRight);
        finish = (Button)findViewById(R.id.done);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void calibRight(View v)
    {
        Intent getRight = new Intent(this,Calibrate.class);

        startActivityForResult(getRight, GET_RIGHT);
    }

    public void startRift(View v)
    {
        Intent startRift = new Intent(this,SearchDevices.class);
        startActivity(startRift);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == GET_RIGHT)
        {
            if(resultCode ==RESULT_OK)
            {
                Toast.makeText(this,"Calibration complete",Toast.LENGTH_LONG).show();
            }
        }

    }


}
